package fancyfoods.api.food;

public interface Food {

    String getName();

    double getPrice();

    int getQuantityInStock();

}

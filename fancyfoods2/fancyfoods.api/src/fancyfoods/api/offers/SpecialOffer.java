package fancyfoods.api.offers;

import fancyfoods.api.food.Food;


public interface SpecialOffer {
	public Food getOfferFood();
	public String getDescription();
}
